define buildkernel::kernel (
  String $kernel_version = 'latest',
  String $kbuser = $buildkernel::kernelprep::kbuser,
  String $kernel_workdir = $buildkernel::kernelprep::kernel_workdir,
  String $kernel_kerneldir = $buildkernel::kernelprep::kernel_kerneldir,
  String $config_version = 'latest',
  String $kernel_name = $title,
  String $kernel_type = 'source',

) {


  require buildkernel::kernelprep

  if ($kernel_type == 'debian') {
     Exec { "generate initram for ${name} ${kernel_version}":
      command     => '/usr/local/bin/generate-debian-initramfs.sh',
      user        => root,
      logoutput   => true,
      require     => [
          File['/usr/local/bin/generate-debian-initramfs.sh'],
      ],
      creates     => "/etc/xen/boot/debian/initrd.img-${kernel_version}",
      environment => ["WORKDIR=${kernel_workdir}",
                      "KERNELVERSION=${kernel_version}",
                      "KERNELCONFIG=${kernel_workdir}/config_${kernel_version}.kconf",
                      "KERNELDIR=${kernel_kerneldir}/debian",
                      "KERNELNAME=${kernel_name}"],

      timeout     => 0,
    }
 }


  if ($type == 'source'){
    file { "${kernel_workdir}/config_${kernel_version}.kconf":
      ensure => present,
      owner  => root,
      group  => $kbuser,
      mode   => '0740',
      source => "puppet:///modules/${module_name}/kconfig/${kernel_version}_${config_version}.kconf",
    }



    Exec { "Check kernel ${kernel_version} with config ${config_version}":
      command     => '/usr/local/bin/check_kernel.sh',
      user        => $kbuser,
      logoutput   => true,
      require     => [
          File['/usr/local/bin/build_kernel.sh'],
          File['/usr/local/bin/check_kernel.sh'],
          File[$kernel_workdir],
          File[$kernel_kerneldir],
          File["${kernel_workdir}/config_${kernel_version}.kconf"],

      ],
      #    onlyif      => "/usr/local/bin/check_kernel.sh",
      environment => ["WORKDIR=${kernel_workdir}",
                      "KERNELVERSION=${kernel_version}",
                      "KERNELCONFIG=${kernel_workdir}/config_${kernel_version}.kconf",
                      "KERNELDIR=${kernel_kerneldir}",
                      "KERNELNAME=${kernel_name}"],

      timeout     => 0,
    }

    Exec { "Build kernel ${kernel_version} with config ${config_version}":
      command     => '/usr/local/bin/build_kernel.sh',
      logoutput   => true,
      user        => $kbuser,
      require     => [
          File['/usr/local/bin/build_kernel.sh'],
          File['/usr/local/bin/check_kernel.sh'],
          File[$kernel_workdir],
          File[$kernel_kerneldir],
          File["${kernel_workdir}/config_${kernel_version}.kconf"],
      ],
    #subscribe   => File["$kernel_workdir/config_$kernel_version.kconf"],
    #refreshonly => true,
    unless        => '/usr/local/bin/check_kernel.sh',

      #    onlyif      => "/usr/local/bin/check_kernel.sh",
      environment => ["WORKDIR=${kernel_workdir}",
                      "KERNELVERSION=${kernel_version}",
                      "KERNELCONFIG=${kernel_workdir}/config_${kernel_version}.kconf",
                      "KERNELDIR=${kernel_kerneldir}",
                      "KERNELNAME=${kernel_name}"],

      timeout     => 0,
    }
  }

}
