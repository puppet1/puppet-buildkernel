class buildkernel::kernelprep (
  String $kernel_workdir = '/var/cache/kernelbuilder',
  String $kernel_kerneldir = '/etc/xen/boot',
  String $signingkey = '647F28654894E3BD457199BE38DBBDC86092693E',
  $signingkeys = [ '647F28654894E3BD457199BE38DBBDC86092693E', 'F81962A54902300F72ECB83AA1FC1F6AD2D09049' ],
  String $kbuser = 'kernelbuilder',
)
 {

  include git

  $packages = [
    'build-essential',
    'python-pip',
    'python3-pip',
    'libncurses5-dev',
    'libssl-dev',
    'libelf-dev',
    'dirmngr',
    'curl',
    'wget',
    'bison',
    'flex',
  #  'gcc-6-plugin-dev',
    'bc',
    'gcc-8-plugin-dev',
    'gcc-7-plugin-dev'
  ]

  ensure_packages($packages, {ensure => 'installed'})


  group { 'kernelbuilder':
  }

  user { 'kernelbuilder':
    gid        => 'kernelbuilder',
    system     => true,
    shell      => '/usr/sbin/nologin',
    managehome => 'true',

  }

  Exec { '/usr/bin/pip3 install git+https://github.com/a13xp0p0v/kconfig-hardened-check':
        user => $kbuser,
  }

  file { '/usr/local/bin/generate-debian-initramfs.sh':
    ensure => present,
    owner  => root,
    group  => kernelbuilder,
    mode   => '0750',
    source => "puppet:///modules/${module_name}/scripts/generate-debian-initramfs.sh",
  }


  file { '/usr/local/bin/build_kernel.sh':
    ensure => present,
    owner  => root,
    group  => kernelbuilder,
    mode   => '0750',
    source => "puppet:///modules/${module_name}/scripts/build_kernel.sh",
  }

  $signingkeys.each |$fp| {

      file { "${kernel_workdir}/${fp}.gpg":
        ensure => present,
        owner  => root,
        group  => kernelbuilder,
        mode   => '0740',
        source => "puppet:///modules/${module_name}/${fp}.gpg",
      }

      #Exec { "/usr/bin/gpg --recv $fp":
      #  user => $kbuser, 
      #}


  }

  Exec { "/usr/bin/gpg --import ${kernel_workdir}/${signingkey}.gpg":
    user    => $kbuser,
    require => [
        File["${kernel_workdir}/${signingkey}.gpg"],
        File['/usr/local/bin/build_kernel.sh'],
        File['/usr/local/bin/check_kernel.sh'],
        File[$kernel_workdir],
        File[$kernel_kerneldir],
    ],
  }



  file { '/usr/local/bin/kernel_functions.sh':
    ensure => present,
    owner  => root,
    group  => kernelbuilder,
    mode   => '0750',
    source => "puppet:///modules/${module_name}/scripts/kernel_functions.sh",
  }

  file { '/usr/local/bin/check_kernel.sh':
    ensure => present,
    owner  => root,
    group  => kernelbuilder,
    mode   => '0750',
    source => "puppet:///modules/${module_name}/scripts/check_kernel.sh",
  }
  file { '/usr/local/bin/test_kernelscript.sh':
    ensure => present,
    owner  => root,
    group  => kernelbuilder,
    mode   => '0750',
    source => "puppet:///modules/${module_name}/scripts/test_kernelscript.sh",
  }
#dirs
  file { '/etc/xen':
    ensure => directory,
  }

  file { $kernel_workdir:
    ensure => directory,
    owner  => root,
    mode   => '0770',
    group  => kernelbuilder,
  }

# this is because apparmor libvirt aa helper, has paths hardcoded in the binary
  file { '/usr/share/qemu-efi':
    ensure => directory,
    mode   => 'a+rx',

  }

  # normally /etc/xen/boot
  file { $kernel_kerneldir:
    ensure => directory,
    owner  => root,
    group  => kernelbuilder,
    mode   => '0774',
  }





}
