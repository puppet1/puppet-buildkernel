#!/bin/bash 
. /usr/local/bin/kernel_functions.sh
set -e

#KEEP=1
#WORKDIR=/root/kernelbuilder
#KERNELDIR=/etc/xen/boot
#KERNELCONFIG=/root/kernel_config_4.18.11.txt
#KERNELVERSION=4.18.11

init

get_kernel 

if check_if_build
then
  exit 0
else
  exit 10
fi
