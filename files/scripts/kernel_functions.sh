function install_dependencies() {
  echo "Installing dependencies..."
  sudo apt -qq update
  sudo apt -qq install curl git wget bison flex
  sudo apt -y -qq install build-essential fakeroot libncurses5-dev libssl-dev ccache libelf-dev dirmngr gcc-6-plugin-dev

  gpg --recv 647F28654894E3BD457199BE38DBBDC86092693E
    gpg --recv F81962A54902300F72ECB83AA1FC1F6AD2D09049
}

function init() {
  echo "Initializing..."
  #WORKDIR=/root/kernelbuilder
#KERNELDIR=/etc/xen/boot
#KERNELCONFIG=/root/kernel_config_4.18.11.txt
#KERNELVERSION=4.18.11
[[ -z "$KERNELDIR" ]] && { echo "KERNELDIR is empty" ; exit 1; }
[[ -z "$KERNELCONFIG" ]] && { echo "KERNELDIR is empty" ; exit 1; }
[[ -z "$KERNELVERSION" ]] && { echo "KERNELDIR is empty" ; exit 1; }
[[ -z "$KERNELNAME" ]] && { echo "KERNELNAME is empty" ; exit 1; }

[[ -z "$WORKDIR" ]] && { echo "KERNELDIR is empty" ; exit 1; }



  #kernel_config=$(ls /boot/config-* | grep generic | sort -Vr | head -n 1)
  kernel_config=$KERNELCONFIG
  kerneldir=$KERNELDIR
        config_shasum=`sha1sum $kernel_config | cut -f 1 -d ' '`  
  echo "config sum is: $config_shasum"
  current_dir=$(pwd)
# working_dir=$(mktemp -d)
  working_dir=$WORKDIR
  kernelname=$KERNELNAME

# mkdir -p $working_dir
  cd "${working_dir}" || exit
  
  ##install_dependencies
}

function get_latest_version() {
  stable_releases="https://mirrors.edge.kernel.org/pub/linux/kernel/v4.x/"
  stable_version=$(curl -s "${stable_releases}" | grep -E -o 'linux-([0-9]{1,}\.)+[0-9]{1,}' | sort -Vr | head -n 1 | cut -d '-' -f 2)
  stable_link="${stable_releases}linux-${stable_version}.tar.xz"
  stable_siglink="${stable_releases}linux-${stable_version}.tar.sign"
  stable_sigfile="linux-${stable_version}.tar.sign"
        stable_file="linux-${stable_version}.tar.xz"  
  imagename="${stable_version}_$config_shasum.vmlinuz"

}

function get_specific_version() {
  stable_releases="https://mirrors.edge.kernel.org/pub/linux/kernel/v4.x/"
# stable_version=$(curl -s "${stable_releases}" | grep -E -o 'linux-([0-9]{1,}\.)+[0-9]{1,}' | sort -Vr | head -n 1 | cut -d '-' -f 2)
stable_version="$1"
  stable_link="${stable_releases}linux-${stable_version}.tar.xz"
  stable_siglink="${stable_releases}linux-${stable_version}.tar.sign"
  stable_sigfile="linux-${stable_version}.tar.sign"
        stable_file="linux-${stable_version}.tar.xz"  
  imagename="${stable_version}_$config_shasum.vmlinuz"
}

function get_grsec_version() {
  grsec_releases="http://ftp.lag/grsec/"
# stable_version=$(curl -s "${stable_releases}" | grep -E -o 'linux-([0-9]{1,}\.)+[0-9]{1,}' | sort -Vr | head -n 1 | cut -d '-' -f 2)
stable_version="$1"
    stable_link="${grsec_releases}linux-${stable_version}_${stable_version}.orig.tar.gz"
    stable_siglink="${grsec_releases}linux-${stable_version}_${stable_version}.orig.tar.gz.gpg"
    stable_file="linux-${stable_version}_${stable_version}.orig.tar.gz"
    stable_sigfile="linux-${stable_version}_${stable_version}.orig.tar.gz.gpg"
  
  imagename="${stable_version}_$config_shasum.vmlinuz"
}

get_kernel() {

if [ "$KERNELVERSION" == "latest" ]
then
  echo 'latest'
  get_latest_version
    TYPE='stable'

elif [[ "$KERNELVERSION" == *"grsec" ]]
then
  echo "grsec"
  get_grsec_version "$KERNELVERSION"
    TYPE='grsec'
else
  echo 'specific'
  get_specific_version "$KERNELVERSION"
fi
  
}

function verify_signature() {
    local sigfile=$1 file=$2 out=
#    if out=$(gpg --status-fd 1 --verify "$sigfile" "$file" 2>/dev/null) &&
#echo "$out" | grep -qs "^\[GNUPG:\] VALIDSIG $fprint " &&
#       echo "$out" | grep -qs "^\[GNUPG:\] TRUST_ULTIMATE\$"; 
 
    if out=$(gpg --status-fd 1 --verify "$sigfile" "$file" 2>/dev/null) &&
       echo "$out" | grep -qs "^\[GNUPG:\] VALIDSIG ";
    then
        return 0
    else
        echo "$out" >&2
        return 1
    fi
}

function download_and_unpack() {

  kernel_version="${stable_version}"
  kernel_name="linux-${kernel_version}"
  #ls -lhtr ${stable_file}
  echo "file: ${stable_file}"
  if [ ! -f "${stable_file}" ]    
  then
    echo "downloading tar"
    wget "${stable_link}"
  fi
  if [ ! -f "${stable_sigfile}" ]   
  then
    echo "downloading sig"
    wget "${stable_siglink}"
  fi
      
    echo "checking sig"
if [[ $TYPE == 'grsec' ]]
then 
   if verify_signature "${stable_sigfile}" "${stable_file}"
    then
        echo "grsec sig verification good"
    else
        echo "grsec sig verification failed"
        exit 23
    fi

    if [ ! -d "${kernel_name}" ]
  then
    echo "unpacking"
    tar -xvf "${stable_file}"
  fi


else    
    echo "unxzing"

  if unxz "${stable_file}"
    then
        echo "unxz ok"
    else
        echo "unxz failed, probably cause was already unxz before"
    fi

    if verify_signature "${stable_sigfile}" "${kernel_name}.tar"
    then
        echo "sig verification good"
    else
        echo "sig verification failed"
        exit 23
    fi
    if [ ! -d "${kernel_name}" ]
  then
    echo "unpacking"
    tar -xvf "${kernel_name}.tar"
  fi

fi
  #tar xvf "${kernel_name}.tar.xz"
  cd "${kernel_name}" || exit
  
}

function build_kernel() {
  kernel_localversion="-localversion"
  
  cp "${kernel_config}" .config

  yes '' | make oldconfig
  make -j 4
  
  #FIXME: make clean
  cp arch/x86/boot/bzImage "$kerneldir/$imagename"
  cd "${current_dir}" || exit
  #FIXME: rm -rf "${working_dir}"

}

function create_links() {
    echo "creating links with kernel nickname"

 if [ -f "$kerneldir/$kernelname.vmlinuz" ]
 then
     rm "$kerneldir/$kernelname.vmlinuz.old" && true
     mv "$kerneldir/$kernelname.vmlinuz" "$kerneldir/$kernelname.vmlinuz.old" && true
 fi

 if [ -f "$kerneldir/$TYPE.vmlinuz" ]
 then
     rm "$kerneldir/$TYPE.vmlinuz.old" && true
     mv "$kerneldir/$TYPE.vmlinuz" "$kerneldir/$TYPE.vmlinuz.old" && true
 fi

ln -s "$kerneldir/$imagename" "$kerneldir/$kernelname.vmlinuz"
ln -s "$kerneldir/$imagename" "$kerneldir/$TYPE.vmlinuz"


}

function check_if_build() {
 if [ -f "$kerneldir/$imagename" ]
 then
   if [ -f "$kerneldir/$kernelname.vmlinuz" ]
   then
     echo "kernel exists: $kerneldir/$kernelname.vmlinuz ($imagename)"
     return 0
   else
     rm "$kerneldir/$kernelname.vmlinuz.old" && true
     mv "$kerneldir/$kernelname.vmlinuz" "$kerneldir/$kernelname.vmlinuz.old" && true
     ln -s "$kerneldir/$imagename" "$kerneldir/$kernelname.vmlinuz"
     echo "kernel existed, but the links where not in place, made them now"
     return 0

   fi
 else
   echo "kernel does not exist"
   return 10
 fi

}

