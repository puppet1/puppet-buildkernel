#!/bin/bash 
. /usr/local/bin/kernel_functions.sh
set -e

KEEP=1
#WORKDIR=/root/kernelbuilder
#KERNELDIR=/etc/xen/boot
#KERNELCONFIG=/root/kernel_config_4.18.11.txt
#KERNELVERSION=4.18.11

init

if ! check_if_build
then
    echo "kernel not build yet, building it"
    get_kernel
        
    #stable_or_mainline_or_repo
    download_and_unpack
    build_kernel
fi



create_links
