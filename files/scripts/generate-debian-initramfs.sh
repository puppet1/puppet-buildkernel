#!/usr/bin/env bash

set -eux
DIR=`mktemp -d`

VERSION="$KERNELVERSION"
KDIR="$KERNELDIR"
#KDIR=/etc/xen/boot/debian
#VERSION="4.19.0-9-cloud-amd64"


cat > $DIR/initramfs.conf <<EOF
MODULES=most
BUSYBOX=auto
KEYMAP=n
COMPRESS=gzip
DEVICE=
NFSROOT=auto
RUNSIZE=10%
EOF

cat > $DIR/modules <<EOF
9p
9pnet
9pnet_virtio
fuse
virtio_net
net_failover
failover
ata_generic
ata_piix
libata
virtio_pci
virtio_ring
scsi_mod
virtio
evdev
serio_raw
virtio_balloon
qemu_fw_cfg
button
virtio_rng
rng_core
ip_tables
x_tables
autofs4
ext4
crc16
mbcache
jbd2
crc32c_generic
fscrypto
ecb
crypto_simd
cryptd 
glue_helper
aes_x86_64
fuse
virtio_blk
virtio_net
net_failover
failover
ata_generic
ata_piix
libata
virtio_pci
scsi_mod
virtio_ring
virtio

EOF

mkdir $DIR/conf.d
mkdir $DIR/scripts

cat > $DIR/conf.d/resume <<EOF
RESUME=none
EOF



apt-get -y install "linux-image-$VERSION"
echo "generating initramfs for the vm"

mkdir -p $KDIR

cp /boot/vmlinuz-"${VERSION}" "$KDIR/vmlinuz-${VERSION}"

mkinitramfs -r /dev/vda -d $DIR -v -o "$KDIR/initrd.img-${VERSION}" "${VERSION}"

