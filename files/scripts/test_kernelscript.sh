#!/usr/bin/env bash

export WORKDIR=/root/kernelbuilder
export KERNELDIR=/etc/xen/boot
export KERNELCONFIG=$WORKDIR/config_latest.kconf
export KERNELVERSION=4.4.162-grsec
export KERNELNAME=ALDSF


/usr/local/bin/check_kernel.sh
/usr/local/bin/build_kernel.sh
